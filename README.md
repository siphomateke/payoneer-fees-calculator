# Payoneer ATM Withdrawal Fees Calculator

Calculates the fees encountered when withdrawing Payoneer funds at an ATM in an international currency.

It basically just calculates the actual amount deducted when including the flat $2.5 and the 3.5% per transaction fees.

![Screenshot of the app in action](./screenshots/screenshot.png)
