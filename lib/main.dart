import 'package:currency_pickers/country.dart';
import 'package:currency_pickers/currency_pickers.dart';
import 'package:flutter/material.dart';
import 'package:forex/forex.dart';
import 'package:payoneer_fees/components/loader.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: Fix primary color not being used
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.deepOrange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.deepOrange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class FeesData {
  Map<String, double> fees;
  Map<String, double> amount;

  FeesData({this.fees, this.amount});
}

class _MyHomePageState extends State<MyHomePage> {
  double _fixedTransactionFee = 2.5;
  double _percentTransactionFee = 3.5;
  String _baseCurrency = 'USD';
  String _currency = 'NAD';
  Country _selectedCountryCurrency;
  final _formKey = GlobalKey<FormState>();
  final _amountController = TextEditingController();
  final _exchangeRateController = TextEditingController();
  final _finalAmountController = TextEditingController();
  final _finalAmountBaseCurrencyController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _selectedCountryCurrency =
        CurrencyPickerUtils.getCountryByCurrencyCode(_currency);
  }

  @override
  void dispose() {
    _amountController.dispose();
    _exchangeRateController.dispose();
    super.dispose();
  }

  Widget _buildCurrencyDialogItem(Country country) {
    return Row(
      children: <Widget>[
        CurrencyPickerUtils.getDefaultFlagImage(country),
        SizedBox(width: 8.0),
        Text("+${country.currencyCode}"),
        SizedBox(width: 8.0),
        Text(country.name)
      ],
    );
  }

  void _openCurrencyPickerDialog() => showDialog(
        context: context,
        builder: (context) => Theme(
          data: Theme.of(context).copyWith(primaryColor: Colors.pink),
          child: CurrencyPickerDialog(
              title: Text('Select currency'),
              titlePadding: EdgeInsets.all(8.0),
              searchInputDecoration: InputDecoration(hintText: 'Search...'),
              isSearchable: true,
              onValuePicked: (Country country) {
                setState(() {
                  _selectedCountryCurrency = country;
                  _currency = _selectedCountryCurrency.currencyCode;
                });
              },
              itemBuilder: _buildCurrencyDialogItem),
        ),
      );

  void _validate() {
    if (_formKey.currentState.validate()) {
      FeesData feesData = calculateFees();
      setState(() {
        _finalAmountController.text =
            feesData.amount[_currency].toStringAsFixed(2);
        _finalAmountBaseCurrencyController.text =
            feesData.amount[_baseCurrency].toStringAsFixed(2);
      });
    }
  }

  Widget _feeCalculatorBuilder(num exchangeRate) {
    if (exchangeRate != null) {
      _exchangeRateController.text = exchangeRate.toString();
    }
    return Column(
      children: <Widget>[
        Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text('Currency: '),
                  FlatButton(
                    child: Row(children: [
                      _buildCurrencyDialogItem(_selectedCountryCurrency),
                      Icon(Icons.arrow_drop_down)
                    ]),
                    onPressed: _openCurrencyPickerDialog,
                  ),
                ],
              ),
              TextFormField(
                decoration: InputDecoration(
                    labelText:
                        'Exchange rate (1 $_baseCurrency to $_currency)'),
                controller: _exchangeRateController,
                keyboardType: TextInputType.number,
                onChanged: (value) => _validate(),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please provide an exchange rate.';
                  } else if (double.tryParse(value) == null) {
                    return 'Please provide a valid exchange rate.';
                  }
                  return null;
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Amount ($_currency)'),
                controller: _amountController,
                keyboardType: TextInputType.number,
                onChanged: (value) => _validate(),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please provide an amount to calculate fees for.';
                  } else if (double.tryParse(value) == null) {
                    return 'Please provide a valid amount.';
                  }
                  return null;
                },
              ),
            ],
          ),
        ),
        TextField(
          decoration: InputDecoration(labelText: 'Total amount ($_currency)'),
          controller: _finalAmountController,
          readOnly: true,
        ),
        TextField(
          decoration:
              InputDecoration(labelText: 'Total amount ($_baseCurrency)'),
          controller: _finalAmountBaseCurrencyController,
          readOnly: true,
        ),
      ],
    );
  }

  Future<num> _getForexRates() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String exchangeRateKey = 'exchangeRate' + _currency;
    num exchangeRate;
    bool refreshRate = false;
    if (!prefs.containsKey(exchangeRateKey)) {
      refreshRate = true;
    } else {
      var exchangeRateLastUpdated = DateTime.fromMillisecondsSinceEpoch(
          prefs.getInt('exchangeRateLastUpdated') ?? 0);
      var age = new DateTime.now().difference(exchangeRateLastUpdated);
      // Update Forex data once a day
      if (age.inDays >= 1) {
        refreshRate = true;
      } else {
        exchangeRate = prefs.getDouble(exchangeRateKey);
      }
    }
    if (refreshRate) {
      exchangeRate = await Forex.fx(
        quoteProvider: QuoteProvider.yahoo,
        base: _currency,
        quotes: <String>[_baseCurrency],
      ).then((value) => value['$_currency$_baseCurrency']);
      prefs.setDouble(exchangeRateKey, exchangeRate);
      prefs.setInt(
          'exchangeRateLastUpdated', new DateTime.now().millisecondsSinceEpoch);
    }
    return exchangeRate;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: Allow refreshing exchange rate
    return Scaffold(
      appBar: AppBar(
        title: Text('Payoneer fees'),
      ),
      body: Container(
        margin: EdgeInsets.all(16),
        child: FutureBuilder(
          future: _getForexRates(),
          builder: (context, snapshot) {
            if (snapshot.hasData || snapshot.hasError) {
              // Don't worry if there were errors. The user can maunally input
              // forex information
              return _feeCalculatorBuilder(snapshot.data);
            } else if (snapshot.hasError) {
              return ErrorLoaderMessage(error: 'Error ${snapshot.error}');
            } else {
              return ProgressLoaderMessage(
                text: 'Loading forex data...',
              );
            }
          },
        ),
      ),
    );
  }

  FeesData calculateFees() {
    // Exchange rate should be 1 _baseCurrency = X currency
    double exchangeRate = double.parse(_exchangeRateController.text);
    double amountInCurrency = double.parse(_amountController.text);
    double amount = amountInCurrency * exchangeRate;
    double totalFee =
        amount * (_percentTransactionFee / 100) + _fixedTransactionFee;
    double finalAmount = amount + totalFee;
    double totalFeeInCurrency = totalFee / exchangeRate;
    double finalAmountInCurrency = amountInCurrency + totalFeeInCurrency;
    return FeesData(
      fees: {
        _baseCurrency: totalFee,
        _currency: totalFeeInCurrency,
      },
      amount: {
        _baseCurrency: finalAmount,
        _currency: finalAmountInCurrency,
      },
    );
  }
}
