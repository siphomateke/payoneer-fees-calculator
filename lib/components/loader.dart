import 'package:flutter/material.dart';

/// Creates a centered message to be shown in a `FutureBuilder` or `StreamBuilder` when loading
/// has failed or is in progress.
class _LoaderMessage extends StatelessWidget {
  /// Widget added above the message.
  final Widget top;
  final String text;

  const _LoaderMessage({
    Key key,
    @required this.top,
    @required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          top,
          Padding(
            padding: EdgeInsets.only(top: 16),
            child: Text(text),
          )
        ],
      ),
    );
  }
}

/// Creates a centered progress indicator with text to be shown in a `FutureBuilder` or `StreamBuilder`.
class ProgressLoaderMessage extends StatelessWidget {
  final String text;

  const ProgressLoaderMessage({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _LoaderMessage(
      top: const SizedBox(
        child: CircularProgressIndicator(),
        width: 60,
        height: 60,
      ),
      text: text,
    );
  }
}

/// Creates a centered loading error message to be shown in a `FutureBuilder` or `StreamBuilder`.
class ErrorLoaderMessage extends StatelessWidget {
  /// The error message to display.
  final String error;

  const ErrorLoaderMessage({Key key, this.error}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _LoaderMessage(
      top: const Icon(
        Icons.error_outline,
        color: Colors.red,
        size: 60,
      ),
      text: 'Error $error',
    );
  }
}

/// Shows a loading indicator and handles loading errors while a `FutureBuilder` or `StreamBuilder`
/// is loading. It can also optionally show empty state when the response is an empty list.
class HandledFutureBuilder<T> extends StatelessWidget {
  final AsyncSnapshot<T> snapshot;
  final Widget Function() buildChild;
  final Widget Function() buildEmptyState;
  final String loadingText;

  const HandledFutureBuilder({
    Key key,
    @required this.snapshot,
    @required this.buildChild,
    this.buildEmptyState,
    this.loadingText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (snapshot.hasData) {
      if (snapshot.data is Iterable && buildEmptyState != null) {
        // TODO: Make sure casting like this is OK
        if ((snapshot.data as Iterable).length > 0) {
          return buildChild();
        } else {
          return buildEmptyState();
        }
      }
      return buildChild();
    } else if (snapshot.hasError) {
      return ErrorLoaderMessage(error: 'Error ${snapshot.error}');
    } else {
      return ProgressLoaderMessage(
        text: '${this.loadingText != null ? this.loadingText : 'Loading'}...',
      );
    }
  }
}
